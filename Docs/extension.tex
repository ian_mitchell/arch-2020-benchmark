%------------------------------------------------------
\section{Problem Extensions}
\label{s:extension}

In this section we sketch out a few extensions to the basic problem,
mostly with the goal of increasing the dimension of the space in which
the RCBRT must be found.  In some cases an analytic solution based on
the solution of the basic problem can be deduced, while in others that
solution only provides some guidance.

It is also possible to break the assumption that the vehicles are
identical by giving them different bounds on their linear and/or
angular velocity inputs.  While potentially valuable for solving real
world pursuit evasion games, we do not consider this option further in
the benchmark because it breaks the derivation of the analytic
solution.

%------------------------------------------------------
\subsection{The No-Heading Projection}
\label{s:extension-no-heading}

The ``extension'' in this section is actually a dimensionality reduction.  As part of a general exploration of how RCBRTs can be approximated by their projections into lower dimensional subspaces of the state space, in~\cite[section~3.5]{MT03} we consider the projection
of the basic problem into the $x_1$--$x_2$ subspace; in other words, the two vehicles know their relative position, but not their heading. Since $x_3 \in [ -\pi, \pi )$, we can model $x_3$ as an input to the two dimensional system in the $x_1$--$x_2$ subspace.

If we treat $x_3$ as an input that the evader can choose, then we are
seeking the set of states from which the evader can be captured for
any possible relative heading.  For identical vehicles the evader can
choose relative heading $x_3 = 0$ and always escape unless the system
starts inside the target set; in other words, the two dimensional
RCBRT is the target set.  So this case is not interesting as a benchmark
problem.

If we treat $x_3$ as an input that the pursuer can choose, then we are
seeking the set of states from which the evader might be captured for
some possible relative heading.  The optimal choice of $x_3$ will
depend on the relative position of the pursuer, but it is easy to see
that the two dimensional RCBRT will be the projection along $x_3$ of
the basic problem's RCBRT.  Consequently, an analytic solution for
this \emph{pursuer's choice} RCBRT can be found by projection of the
solution described in section~\ref{s:basic-analytic}.

This simplification can be used in concert with the extensions in the
remainder of this section to provide RCBRT problems in a range of
dimensions.  We will note in passing that problem versions which
project out either $x_1$ or $x_2$ yield RCBRTs which are either
trivial or unbounded, so these versions do not appear to be interesting as benchmark problems either.

%------------------------------------------------------
\subsection{Absolute Coordinates}
\label{s:extension-absolute}

We used relative coordinates for the two vehicles in
section~\ref{s:basic} because we could reduce the dimension of the
state space from six to three.  But if we want a higher dimensional
version of the benchmark, we can work directly in the absolute
coordinates of the two vehicles.  Letting $x_1$, $x_2$ and $x_3$ be
the coordinates of the evader and $x_4$, $x_5$ and $x_6$ the
coordinates of the pursuer, we can substitute
\[
  \begin{aligned}
    \set X &= \R^2 \times [ -\pi, \pi ) \times \R^2 \times [ -\pi, \pi ) \\
    \set T &= \left\{ x \in \set X \, \left| \, 
                      \sqrt{(x_3 - x_1)^2 + (x_4 - x_2)^2} \leq \beta
                      \right. \right\}.
  \end{aligned}
\]
for~\eqref{e:domain-basic} and~\eqref{e:target-basic} respectively,
and
\begin{equation} \label{e:dynamics-absolute}
  \dot x
      = \frac{d}{dt} \bma x_1 \\ x_2 \\ x_3 \\ x_4 \\ x_5 \\ x_6 \ema
      = \bma v \cos x_3 \\ v \sin x_3 \\ \omega_e \\
             v \cos x_6 \\ v \sin x_6 \\ \omega_p \ema
\end{equation}
for~\eqref{e:dynamics-basic} to create a benchmark problem in six
state dimensions. Analytic slices of the RCBRT for any fixed evader
state can be evaluated by transforming into relative coordinates and
using the results from section~\ref{s:basic-analytic}.  Furthermore, a
four dimensional problem can be defined by using absolute coordinates
with the no-heading projection from
section~\ref{s:extension-no-heading}.

%------------------------------------------------------
\subsection{More Vehicles}
\label{s:extension-multiple}

An easy way to increase the dimension of this pursuit-evasion game is
to introduce more vehicles.  Writing down the dynamics is a
straightforward replication of~\eqref{e:dynamics-basic}
or~\eqref{e:dynamics-absolute} depending on whether relative or
absolute coordinates are used.  The definition of
RCBRTs~\eqref{e:rcbrt-defn} requires that each vehicle be assigned to
either the evader or pursuer team.  Most importantly, some care must
be taken to create a target set whose RCBRT will have appropriate
semantics.

\paragraph{Single evader.}  This case is simplest to formulate in
either relative coordinates (with the evader at the origin) or
absolute coordinates.  The target set is the union (if any pursuer
can capture) or intersection (if all pursuers must capture
simultaneously) of states where a pursuer is sufficiently close to the
evader; for example, to encode the case where either of two pursuers
can capture in relative coordinates, use
\begin{equation} \label{e:target-single-evader}
      \set T = \left\{ x \in \set X \, \left| \, 
                       \min \left(\sqrt{x_1^2 + x_2^2},
                                  \sqrt{x_4^2 + x_5^2} \right) \leq \beta
                       \right. \right\},
\end{equation}
where $(x_1, x_2, x_3)$ are the relative coodinates of the first
pursuer and $(x_4, x_5, x_6)$ are the relative coordinates of the
second pursuer.  Encoding other semantics requires additional
capabilities; for example:
\begin{compactitem}
\item State constraints (aka \emph{avoid} sets) allow problems in
  which the pursuers cannot get too close together, as well as
  problems in which there are obstacles in the environment.
\item A hybrid state model can capture problems in which more than one
  pursuer must capture but they can do so sequentially rather than all
  at once.  \todoIM{It would be good to provide an example HA.}
\end{compactitem}

\paragraph{Single pursuer.}  This case is also easy to formulate in
either relative coordinates (with the pursuer at the origin) or
absolute coordinates.  Requiring a single pursuer to capture multiple
evaders simultaneously seems overly challenging, but a target allowing
the pursuer to capture any single evader can easily be encoded; for
example, using~\eqref{e:target-single-evader} for the situation where
there are two evaders and the state variables are the coordinates of
those evaders relative to the pursuer at the origin.  If the evaders
must avoid one another, and we are willing to count an evader
collision as a pursuer win, then the target can be defined as, for example,
\[
      \set T = \left\{ x \in \set X \, \left| \, 
                       \min \left(
                           \sqrt{x_1^2 + x_2^2},
                           \sqrt{x_4^2 + x_5^2},
                           \sqrt{(x_1 - x_4)^2 + (x_2 - x_5)^2}
                       \right) \leq \beta
                       \right. \right\},
\]
where the third term in the minimum encodes that the
evaders have gotten too close together.  A hybrid state model can be
deployed if the pursuer must capture (a subset of) evaders in some
sequence.

\paragraph{Multiple pursuers and evaders.} Absolute coordinates make more
sense in this case because there is no unique vehicle.  Encoding an ``any
pursuer captures any evader'' winning condition is straightforward,
but an ``every evader is simultaneously captured'' winning condition
can be managed; for example, with two pursuers and two evaders
\[
    \set T = \left\{ x \in \set X \, \left| \,
                     \max \left[
                       \begin{gathered}
                       \min \left(
                           \sqrt{(x_1 - x_7)^2 + (x_2 - x_8)^2},
                           \sqrt{(x_1 - x_{10})^2 + (x_2 - x_{11})^2}
                       \right), \\
                       \min \left(
                           \sqrt{(x_4 - x_7)^2 + (x_5 - x_8)^2},
                           \sqrt{(x_4 - x_{10})^2 + (x_5 - x_{11})^2}
                       \right)
                       \end{gathered}
                       \right] \leq \beta
                       \right. \right\},
\]
where
\begin{compactitem}
\item $(x_1, x_2, x_3)$ and $(x_4, x_5, x_6)$ are the two evaders'
  absolute coordinates.
\item $(x_7, x_8, x_9)$ and $(x_{10}, x_{11}, x_{12})$ are the two
  pursuers' absolute coordinates.
\item The first minimum encodes the fact that the first evader is
  captured by one of the pursuers, while the second minimum encodes
  the fact that the second evader is captured by one of the pursuers.
  Note that one pursuer can capture both evaders.
\item The maximum encodes the fact that both evaders must be captured
  simultaneously.
\end{compactitem}
Enumeration of all winning simultaneous combinations for the pursuers,
including cases where evader collision causes a pursuer win, can be
encoded similarly, while winning sequential combinations can be
encoded with a hybrid state space.

\paragraph{Analytic solution.} The higher dimension and the interactions
among the vehicles makes it difficult to imagine how Merz's approach
could be extended to provide analytic solutions to these cases;
however, we can under or perhaps over approximate some of these
solutions using the solution from section~\ref{s:basic-analytic} and
pairwise consideration of the pursuers and evaders.  For example, if
``capture any evader'' is a winning condition for the pursuer(s), then
every pursuer-evader pair whose relative coordinates fall within the
RCBRT for the basic problem is within the RCBRT for the multivehicle
problem; however, if there are multiple pursuers then it may be
possible for them to coordinate and herd the evader(s) to force a win,
but this strategy would not be captured by pairwise consideration.
Consequently, the analytic solution for the basic problem can generate
an under approximation for this multivehicle case.  \todoIM{The
  converse ``capture all evaders simultaneously'' winning condition
  doesn't immediately lead to a pairwise over approximation: Consider
  a large circle of pursuers spaced $\beta$ apart, and 2+ evaders deep
  within the circle.  The evaders are not within any pairwise RCBRT,
  but the pursuers can shrink the circle until a simultaneous capture
  occurs.}

\ignore{
One way to think about these cases is to view the consideration of
pursuer-evader pairs as a slice through the full state space which
chooses a particular value for those states which are not part of the
pair under consideration.  Some such slices may be within the full
RCBRT, while others may not.  Following the intuition from
section~\ref{s:extension-no-heading} and depending on how the winning
conditions are structured, we can deduce that
\begin{compactitem}
\item The pursuer(s) can choose a slice so that the full state is
  within the full RCBRT, in which case the pairwise solution is an
  under approximation.
\item The evader(s) can choose a slice so that the full state is not
  within the full RCBRT, in which case the pairwise solution is an
  over approximation.
\end{compactitem}
Each of these cases is illustrated by an example above, but there are
also winning condition structures in which neither of these cases
holds and then the basic problem's solution may not be useful.
\todoIM{Is that claim true?  Can we find an example of the latter?}
}

%------------------------------------------------------
\ignore{
% Not worth keeping unless an interesting case can be found.
\subsection{Reeds-Shepp Vehicles}
\label{s:extension-rs}

Returning to the two vehicle case, the extension to Reeds-Shepp motion
gives the vehicle(s) an added input whose signal must be
characterized.  If we merely constrain the linear velocity input(s) to
be measurable, then we can replicate any intermediate linear velocity
and the model is effectively the kinematic unicycle considered in
section~\ref{s:extension-unicycle}.  If the pursuer is allowed a
single opportunity to choose velocity at the beginning of the game, we
can model this choice as allowing the pursuer to choose to start the
game at either the initial relative heading $x_3(0)$ or its complement
$x_3(0) - \pi$ (shifted if necessary back into the range $[-\pi,
  \pi)$).
}

%------------------------------------------------------
\subsection{Unicycle Kinematics and/or Dynamics}
\label{s:extension-unicycle}

The dynamics for the kinematic unicycle model are still given by~\eqref{e:dynamics-basic}, except that now the evader has two inputs $(v_e, \omega_e)$ and the pursuer has two inputs $(v_p, \omega_p)$.

For vehicles with non-trivial inertia the obvious extension beyond a kinematic model is to use linear accelerations as the inputs:
\[
    \dot x
      = \frac{d}{dt} \bma x_1 \\ x_2 \\ x_3 \\ x_4 \\ x_5 \ema
      = \bma -x_4 + x_5 \cos x_3 + \omega_e x_2 \\
              x_5 \sin x_3 - \omega_e x_1 \\
              \omega_p - \omega_e \\
              a_e \\ a_p \ema,
\]
where the evader's and pursuer's linear velocities are now part of the state vector ($x_4$ and $x_5$ respectively), and the linear accelerations ($a_e$ and $a_p$) are the inputs (along with $\omega_e$ and $\omega_p$).  A further extension shifts the angular velocities into the state vector and introduces the angular accelerations as inputs (although this level of model is less often necessary because many robots have comparatively small moments of inertia).  It is therefore possible to create versions of RCBRT problems in three, five or seven dimensions in relative coordinates (six, eight or ten dimensions in absolute coordinates) with two inputs for each of the evader and pursuer.

The kinematic and particularly the dynamic unicycle are more realistic
models of real world robots, so efficient methods of approximating and
representing their RCBRTs would be highly beneficial.  Unfortunately,
there is no obvious way to extend Merz's results to these higher
dimensional cases, so an analytic solution is not available.

