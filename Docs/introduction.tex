%------------------------------------------------------
\section{Introduction}
\label{s:introduction}

The controlled invariant set is a common tool used to prove that
systems can be controlled to remain within some set of states, and the
robust controlled invariant set is a version in which the invariance
can be shown despite perturbation of the dynamics.  Similar problems
have been studied in reachability, although differentiating between
research on reachability algorithms which work for systems with no
inputs, with a single input, or with two adversarial inputs has been a
challenging task because all use the generic term ``reachability''.
In~\cite{mitchell11} we proposed notation to differentiate these
cases---as well as whether the set includes states reachable over an
interval of time or at a single time, and whether the computation
proceeds forward in time from an initial set or backward in time from
a target set---but the notation is cumbersome, and unnecessary when
considering algorithms which focus on solving a single version of the
problem.  More importantly, notation cannot easily appear in titles,
abstracts or introductions, making it hard for others to easily
identify which particular problem is being solved in a given paper.

Increasing use of the word ``tube'' for the case in which reachability
is evaluated over an interval of time has helped the situation, so to
differentiate between reachability problems with various treatments of
inputs we propose to follow the lead of robust controlled invariant
sets and use the phrase ``robust controlled'' to identify the category
of reachability problems with adversarial inputs.  In particular, in
this paper we propose a benchmark problem for \emph{robust controlled
  backward reach tubes} (RCBRTs), in which we seek to compute the set
of states giving rise to trajectories which can be controlled by the
input(s) of one player to reach a given target set at a some time
during a given time interval in a manner robust to the disturbance of
the second player's input(s) on the dynamics.  RCBRTs clearly share
features with robust controlled invariant sets as well as
discriminating kernels from viability theory~\cite{ABSP11}, with the
key difference being the presence of a terminating target set (similar
to viability theory's capture basins).

Impressive advances have been demonstrated in the scalability and
accuracy of reachability algorithms for systems with fewer inputs over
the past decade; for example~\cite{BTJ19} demonstrates a previously
unimaginable billion dimensional problem, albeit for a system with
linear dynamics and no inputs.  Much less progress has been made on
RCBRTs; for example,~\cite{KMOD15} demonstrates an analysis on a
twelve dimensional linear system with adversarial inputs.

Unlike reachability analyses without adversarial inputs, RCBRTs do not
appear to be amenable to efficient sampling approaches because (as
demonstrated below) distinct optimal trajectories may arise from a
single point in the state space and/or lead to a single point in the
target set; consequently, two-point boundary value based or
optimization based sampling may miss such trajectories, and dense
sampling of the competing inputs from all possible initial states is
generally infeasible.

And yet RCBRTs have considerable potential in analysis and synthesis
of safe controllers which will drive a system toward (or away from)
target sets despite uncertainty in the model, environment, or actions
of other agents.  Examples beyond the collision avoidance problem
considered here include automated delivery of
anesthesia~\cite{YHMD19}, learning a Gaussian process model of drone
flight while maintaining safety~\cite{FAZKGT18}, and analysis of the
user interface of an automated landing system~\cite{OMBT08}.

With this benchmark proposal we hope to drive interest in developing
more general and still scalable algorithms for RCBRTs.  The
contributions of this benchmark proposal are to
\begin{compactitem}
\item Describe an RCBRT whose boundary can be sampled analytically to
  arbitrary density.  The analytic solution can be used to validate
  and analyze the accuracy of potential solutions.  The RCBRT:
  \begin{compactitem}
  \item Is derived from a collision avoidance problem involving
    two Dubins cars.
  \item Features nonlinear dynamics with bounded, adversarial inputs.
  \item Is nonconvex with a surface that is non-differentiable in
    places.
  \item Can be characterized in either three or six dimensions
    depending on whether a relative coordinate frame is used.
  \item Includes pairs of initial set / RBRT points connected by
    multiple distinct optimal trajectories.
  \end{compactitem}
\item Describe extensions to the basic problem for which the system
  dimension is larger.  For some of these extensions, the analytic
  solution for the basic problem can be used to find under or over
  approximations of the true RCBRT.
\end{compactitem}
The paper is organized as follows: Section~\ref{s:basic} describes the problem for which the (almost) analytic solution is known, and how that solution can be characterized.  Section~\ref{s:extension} describes a number of extensions to the basic problem and how they relate to the (almost) analytic solution.  Section~\ref{s:hj} briefly describes how these problems can be formulated as Hamilton-Jacobi-Isaacs PDEs whose solutions can be numerically approximated, although these techniques scale poorly with dimension.  Source code can be found at \url{https://bitbucket.org/ian_mitchell/arch-2020-benchmark}.