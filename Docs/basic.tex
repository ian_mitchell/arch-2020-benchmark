%------------------------------------------------------
\section{The Basic Version}
\label{s:basic}

In this section we describe the problem which leads to an RCBRT in
three dimensions and summarize an analysis which allows us to write
closed form solutions for points on the boundary of the tube.  All of
these elements have been examined in prior publications, so the
purpose of this section is to provide a cohesive overview and
justification for use as a benchmark rather than the details of the
constructions; however, those details are available in the citations
for interested readers.

%------------------------------------------------------
\subsection{The Problem}
\label{s:basic-problem}

The motion of a kinematic unicycle model is governed by the ODE
\[
    \dot x
      = \frac{d}{dt} \bma x_1 \\ x_2 \\ x_3 \ema
      = \bma v \cos x_3 \\ v \sin x_3 \\ \omega \ema
\]
where
\begin{compactitem}
  \item The \emph{state} $x \in \R^2 \times [ -\pi, \pi )$ consists of
    the position in the plane $(x_1,x_2)$ and heading $x_3$.
  \item The \emph{inputs} are the linear velocity $v \in [ \underline
    v, \overline v ] \subseteq \R$ and angular velocity $\omega \in [
    -\overline \omega, \overline \omega ] \subseteq \R$.  Note that we
    assume symmetry of the angular velocity range.
\end{compactitem}

In a Dubins car model~\cite{dubins57} the linear velocity is a fixed
constant $v = \underline v = \overline v$, often chosen as $v = +1$,
while angular velocity is allowed to vary.  In a Reeds-Shepp car
model~\cite{RS90} the linear velocity is chosen from two symmetric
fixed values; for example, $v \in \{ -1, +1 \}$.  Both cases can
equivalently be defined as a car with limited turn radius (equal to
$\overline \omega^{-1}$ for unit linear velocity), whereas the
unicycle can turn in place if the linear velocity can be set to zero.

Numerous works have examined the reach sets of individual vehicles
traveling under various versions of unicycle dynamics---for example,
in~\cite{PT09} the authors examine the case where $\overline v = +1$
and $\underline v$ is allowed to vary---but in these cases all
nondeterminism in the input signal(s) seeks the same outcome
(typically to make the reach set larger).

In this paper we consider an adversarial game scenario involving two
identical vehicles in which the input(s) of one are trying to bring
the vehicles close together while the input(s) of the other are trying
to keep them apart.  It can be thought of as a version of the
homicidal chauffeur problem~\cite{isaacs99}, but with each of the two
agents having identically limited unicycle dynamics rather than one
being a car and the other a pedestrian.

In order to distinguish between the two vehicles we will call the one
seeking to drive the vehicles together the \emph{pursuer} and the one
seeking to keep them apart the \emph{evader}.  In this basic problem
we will fix the linear velocity $v = +1$ (so the vehicles behave as
Dubins cars) and the bounds on the angular velocity as $\overline
\omega = +1$.  We will declare that the pursuer wins if the vehicles
pass within distance $\beta > 0$ of each other in the plane,
regardless of their headings; otherwise, the evader wins.

Because the winning conditions depend only on the relative position of
the vehicles, we can recast the problem in relative coordinates.  For
reasons involving the algorithm discussed in section~\ref{s:hj}, we
choose to put the evader at the origin of the relative coordinate
system; however, the analytic solution can be derived with either
vehicle at the origin (see section~\ref{s:basic-analytic}).

\begin{figure}[t]
	\begin{centering}
	\includegraphics[width=0.7\textwidth]{air3d-coordinates}
	\caption{Relative coordinate system with the evader at the
          origin.}
	\label{f:relative-coordinates}
	\end{centering}
\end{figure}

In the relative coordinate system shown in
figure~\ref{f:relative-coordinates}, the dynamics of the system are
given by the nonlinear ODE
\begin{equation} \label{e:dynamics-basic}
    \dot x
      = \frac{d}{dt} \bma x_1 \\ x_2 \\ x_3 \ema
      = \bma -v_e + v_p \cos x_3 + \omega_e x_2 \\
              v_p \sin x_3 - \omega_e x_1 \\
              \omega_p - \omega_e \ema,
\end{equation}
where
\begin{compactitem}
\item $( x_1, x_2) \in \R^2$ is the relative position in the plane.
\item $x_3 \in [ -\pi, \pi )$ is the relative heading,
\item $v_e$ and $v_p$ are the linear velocities of the evader and
  pursuer respectively.  These velocities are constant and equal so
  they are not considered inputs in this version of the problem.
\item $\omega_e$ and $\omega_p$ are the angular velocities of the
  evader and pursuer respectively.  These are drawn from the same
  interval $[ -\overline \omega, \overline \omega ]$ and are the
  inputs for this version of the problem.
\end{compactitem}
We will define the domain in which we compute the RCBRT as
\begin{equation} \label{e:domain-basic}
  \set X = \R^2 \times [ -\pi, \pi )
    \quad \text{(or } \R^2 \times \mathbb{S}^1 \text{)} 
\end{equation}
using this relative coordinate system.

The backward reach tube that we seek to compute represents the set of
states from which the pursuer can drive the system trajectory into the
\emph{target set}
\begin{equation} \label{e:target-basic}
  \set T = \left\{ x \in \set X \, \left| \, 
                   \sqrt{x_1^2 + x_2^2} \leq \beta \right. \right\}.
\end{equation}
In the three dimensional state space, $\set T$ is a cylinder because
it does not depend on relative heading $x_3$.  Figure~\ref{f:rcbrt}
shows $\set T$.

We consider the set to be computed an RCBRT because the pursuer will
use its input $\omega_p$ to try to control the system state toward
$\set T$, but must do so in a way which is robust to the efforts of
the evader to steer away through its input $\omega_e$.  Because we are
operating in continuous time, the question arises of what each vehicle
knows about the other's choices.  We will give any potential
informational benefit to the pursuer by allowing it to use a
nonanticipative strategy.  The intuition for how this choice affects
the problem is that at a given time instant the evader knows the
current state of the system and how the pursuer will respond to any
given evader input, while the pursuer knows both the state and the
evader's actual chosen input.  The notation for non-anticipative
strategies becomes somewhat cumbersome, so the interested reader is
referred to~\cite[section~2]{MBT05} for more details.

\begin{remark} \label{r:separable}  
  The dynamics~\eqref{e:dynamics-basic} are separable, meaning they
  can be written in the form
  \[
      \dot x = f_0(x) + f_e(x, \omega_e) + f_p(x, \omega_p),
  \]
  where each of the component terms depends on no more than one of the
  inputs $\omega_e$ and $\omega_p$.  While the details of the
  non-anticipative strategy information pattern is important to
  achieve mathematical rigour for cases with general dynamics, for
  separable dynamics it is equivalent to the more intuitive state
  feedback information pattern; in other words, each player's
  instantaneously optimal input does not depend on the other player's
  instantaneous choice.
\end{remark}

\begin{figure}[t]
	\begin{centering}
	\includegraphics[width=0.9\textwidth]{basic_surface}
	\caption{Two views of the surface of the RCBRT (red shape) for the basic problem.  On the left, the RCBRT surface is partially transparent and the target set is shown as a solid blue cylinder.  This set is the same as that shown in~\cite[Figure~3]{MBT05},~\cite[Figure~3.5]{mitchell02} and~\cite[Figure~8]{mitchell01}, but with relative heading in $[-\pi, \pi)$ (instead of $[ 0, 2\pi )$) and different values for $\beta$, $v_e$ and $v_p$.}
	\label{f:rcbrt}
	\end{centering}
\end{figure}

The RCBRT is formally defined as
\begin{equation} \label{e:rcbrt-defn}
    \reachSet{[-T,0]}{\set T}
      = \{ x \in \set X \mid
           \exists \omega_p(\cdot), \forall \omega_e\sig,
           \exists s \in [ -T, 0 ], x(s) \in \set T \}
\end{equation}
where
\begin{compactitem}
\item The (backward) horizon is $T > 0$.
\item An input signal $u\sig : [T, 0] \to \set U$ is drawn from the
  set of measurable functions mapping a time to a valid input value.
\item With an abuse of notation, the pursuer's nonanticipative
  strategy $\omega_p(\cdot)$ is a mapping from evader input signals to
  pursuer input signals.
\item The evader's input signal $\omega_e\sig$ is just a regular input
  signal.
\end{compactitem}

The definition~\eqref{e:rcbrt-defn} may appear to give the information
benefit to the evader since $\omega_e\sig$ is picked last, while at
the same time restricting both input signals to be open loop since
they are both functions mapping time to input value.  However
\begin{compactitem}
\item Because $\omega_p(\cdot)$ is a strategy it can respond to any
  particular choice of $\omega_e(t)$ instantaneously even though
  $\omega_e\sig$ is chosen later.
\item Because $\omega_p(\cdot)$ is chosen first, both players know how
  the pursuer will respond to any given choice by the evader; hence
  both players can deduce the current state at any given time and
  thereby have access to the necessary information to build closed
  loop input signals.
\end{compactitem}

Measurable functions and non-anticipative strategies may seem like
mathematically heavy artillery when we might instead desire to impose
more practical constraints on the input signals for real problems,
such as piecewise constant or linear state feedback.  In fact, the
optimal input signals needed for the analytic solution of this basic
problem turn out to be piecewise constant (see
section~\ref{s:basic-analytic}), but we call out the big guns here in
order to enable the approximation of this RCBRT and the RCBRTs for
extended versions of this problem through viscosity solutions of HJI
PDEs in section~\ref{s:hj}.

At the same time, the need for these precise definitions should serve
as a warning that the definition and solution of this RCBRT is
mathematically delicate in places.  In particular, the target set
$\set T$ is the closure of an open set, and in this case it can be
shown that the RCBRT is also a closed set (for example,
see~\cite{MBT05}).  Given that the definitions all have obvious
complements, one might be tempted to deduce that the evader's RCBRT is
precisely the complement of the pursuer's RCBRT~\eqref{e:rcbrt-defn};
however, the story is more complicated,\footnote{In
  particular,~\cite[Lemma~8, case~2]{MBT05} has a circular dependency
  which breaks the proof (much thanks to Andre Platzer for spotting
  this error), but the version of the same lemma
  in~\cite[section~2.4.3]{mitchell02} works fine with a slightly
  weakened claim containing a strict inequality.} and the interested
reader can find a rigorous treatment in~\cite[section~4]{CQSP99}.
Fortunately, those interested in computing over and under
approximations numerically can use the benchmark formulation above and
for the most part avoid these mathematical technicalities.

%------------------------------------------------------
\subsection{An (Almost) Analytic Solution}
\label{s:basic-analytic}

In~\cite{merz72} the author deduces the optimal trajectories for the
problem described above, albeit for the relative coordinate system
with the pursuer at the origin.  In~\cite{mitchell01} we write out
Merz's equations in detail for his case, and then replicate the
analysis for the relative coordinate system with the evader at the
origin.  We provide accompanying \matlab\ code which regenerates the
figures and can compute points on the boundary of the RCBRT to an
accuracy comparable with the available implementation of the basic
trigonometric functions.

Given the detailed exposition available in these manuscripts, the
purpose of this section is merely to highlight interesting
characteristics and features of this analytic solution, and the
interested reader can find the details in~\cite{merz72,mitchell01}.

In the original paper, Merz parameterizes trajectory terminal points
on the boundary of the cylindrical target set $\set T$, and then
claims that points on the \emph{barrier} (the portion of the boundary
of the RCBRT which is not also the boundary of the target set) can be
described as the initial conditions of one or more optimal
trajectories made up of segments along which both inputs are fixed.
Three types of parameterized segment exist, each having two versions
depending on the choice of input:
\begin{compactitem}
\item Opposite inputs: In this type $\omega_e = -\omega_p$, and the
  two versions depend on whether $\omega_e = +1$ or $\omega_e = -1$.
\item Same inputs: In this type $\omega_e = \omega_p$, and the two
  versions depend on whether $\omega_e = +1$ or $\omega_e = -1$.
\item Pursuer not turning: In this type $\omega_p = 0$, and the two
  versions depend on whether $\omega_e = +1$ or $\omega_e = -1$.
\end{compactitem}

The boundary of the RCBRT can be sliced along fixed values of $x_3$
(horizontal slices in figure~\ref{f:rcbrt}), and the boundary of each
slice divided into three smooth curves: Some half of the circle which
is the corresponding slice of the target set $\set T$, and two curves
which start off tangentially from the two ends of the half circle and
meet at a \emph{crossover point} where the barrier is not
differentiable.

The first step to compute points lying on a particular slice of the
barrier is to determine the crossover point.  There are two possible
optimal trajectories arising from this point on the left and two on
the right depending on which trajectory segments are involved, and the
crossover point can be found by equating each possible pair of left
and right, and solving the resulting two equations (one each for
coordinates $x_1$ and $x_2$ since the slice $x_3$ is fixed) for two
unknowns (the time durations of the two trajectories) to find the
combination which is physically realizable.

\begin{figure}[t]
	\begin{centering}
	\includegraphics[width=0.9\textwidth]{evader_slices}
	\caption{Two views of slices at fixed $x_3$ of the boundary of
          the RCBRT with $\beta = 0.5$ and the evader at the origin.
          Note that the $x_1$ and $x_2$ coordinates are swapped
          compared to the coordinate system in
          figure~\ref{f:relative-coordinates}.
          The large circles are slices of the target set
          $\set T$.  The star symbols are the crossover points for
          each slice, the small circle symbols are the intersection of
          the barrier and the target set slices, and the triangle /
          square symbols denote points on the barrier slices where the combination of optimal trajectory segments to reach the barrier changes.  Figure
          taken from~\cite[figure~6]{mitchell01}.}
	\label{f:barrier-slices-evader}
	\end{centering}
\end{figure}

\begin{figure}[t]
	\begin{centering}
	\includegraphics[width=0.9\textwidth]{pursuer_slices}
	\caption{Two views of slices at fixed $x_3$ of the boundary of
          the RCBRT with $\beta = 0.5$ and the pursuer at the origin.
          The labelling is the same as that in
          figure~\ref{f:barrier-slices-evader}.  Figure taken
          from~\cite[figure~5]{mitchell01}, and the left subplot
          recreates~\cite[figure~7]{merz72}.}
	\label{f:barrier-slices-pursuer}
	\end{centering}
\end{figure}

The remainder of the points on the barrier arise from one of eight
possible combinations of trajectory segments, with a left-right
asymmetry arising from the complementary shapes of the RCBRT above and
below $x_3 = 0$.  Each combination has a free parameter, so by
sampling these free parameters and the slices in $x_3$ it is possible
to generate points on the barrier to an arbitrary density.

Explicit, closed form equations for all of the points on the barrier
can be written except for the crossover points.  The crossover points
have closed form equations, but they must be solved numerically
because they are not explicit and involve nonlinear trigonometric
terms; consequently, we call Merz's solution ``almost analytic.''  In
practice the equations can be solved to arbitrary accuracy using
appropriate numerical methods, and are solved to nearly IEEE double
precision floating point accuracy in the accompanying \matlab\ code.

Figure~\ref{f:barrier-slices-evader} shows two views of slices of the
RCBRT at
\[
    x_3 = -\left(\frac{k}{6}\right)\pi
    \qquad \text{for } k = \{ 1, 2, 3, 4, 5, 6 \}
\]
with the evader at the origin and for $\beta = 0.5$.  Note that the
$x_1$ and $x_2$ axes are swapped for figures in this section compared
to the axes defined in figure~\ref{f:relative-coordinates} (the
swapped axes agree with the coordinate system from~\cite{merz72}).
The $x_3$ axis is also labelled in degrees rather than radians.  The
figure shows that the RCBRT is not smooth at the crossover points.  It
is also possible to deduce that the RCBRT is nonconvex: Consider the
line joining $x = (0, 0.5, 0)$ on the top slice of the target set to
the point $x = (0, 2.9, -\pi)$ just below the crossover point for the
bottom slice.  This line will pass through the point $x = (0, 0.9,
-\pi/6)$, which is clearly not within the RCBRT slice for $x_3 =
-\pi/6$.

For comparison, figure~\ref{f:barrier-slices-pursuer} shows two views
of the same slices of the RCBRT with the pursuer at the origin.

\begin{figure}[t]
	\begin{centering}
	\includegraphics[width=0.9\textwidth]{pursuer_left_barrier}
	\caption{Two views of a coarse sampling of trajectories whose
          endpoints form a slice of the left barrier of the RCBRT with
          $\beta = 0.5$ and the pursuer at the origin.  Note that the
          set of trajectories starting from points closer to the
          crossover point all share a terminal trajectory segment.
          Figure taken from~\cite[figure~4]{mitchell01}.}
	\label{f:barrier-trajectory-samples}
	\end{centering}
\end{figure}

Figure~\ref{f:barrier-trajectory-samples} shows a coarsely sampled set
of optimal trajectories leading to one side of the (pursuer at the
origin) barrier for $x_3 = -5 \pi / 6$.  The set of trajectories
closer to the origin terminate at different points on the target set
and comprise of only a single optimal trajectory segment.  The set of
trajectories closer to the crossover point all comprise of two optimal
segments, the second of which is shared and terminates at a single
point on the target set.  Finally, no portion of this side of the
barrier for this slice is itself an optimal trajectory segment.

\begin{figure}[t]
	\begin{centering}
	\includegraphics[width=0.9\textwidth]{pursuer_crossover}
	  \caption{Left and right trajectories leading from crossover
            points for slices $x_3 = -5 \pi / 6$ and $x_3 = -\pi / 2$
            of the RCBRT with $\beta = 0.5$ and the pursuer at the
            origin.  Slices of the target cylinder are also shown at
            the terminal points of the trajectories.  For the slice at
            $x_3 = -\pi / 2$, the portion of the left barrier between
            the star symbol at the crossover point and the square
            symbol is part of the left trajectory.  Figure taken
            from~\cite[figure~3]{mitchell01}.}
	\label{f:barrier-trajectory-crossover}
	\end{centering}
\end{figure}

Figure~\ref{f:barrier-trajectory-crossover} shows the two trajectories
leading from the crossover points for two distinct slices (again for
the case with the pursuer at the origin).  Notably, the trajectories
leading from the crossover point for the slice at $x_3 = -5 \pi / 6$
not only diverge at first, but they then converge to the same terminal
point on the target set.  The slice at $x_3 = - \pi / 2$ shows
distinct features.  First, the trajectories leading from the crossover
point terminate at different points on the target set.  Second, a
portion of the left barrier is composed of an optimal trajectory
segment.

\begin{remark} \label{r:challenges}
  From the problem definition in section~\ref{s:basic-problem} and the
  (almost) analytic solution described here we can identify a number
  of features which make this reachability problem challenging:
  \begin{compactitem}
  \item The dynamics~\eqref{e:dynamics-basic} are nonlinear, with
    $x_1$ and $x_2$ coupled to all dimensions except themselves.
  \item The domain~\eqref{e:domain-basic} is not of the form $\R^d$
    for some $d$ because $x_3$ is periodic.
  \item The cylindrical target~\eqref{e:target-basic} couples $x_1$
    and $x_2$, although it could be over or under approximated by a
    square prism if decoupling is desired.
  \item There are two inputs with competing goals.  The dynamics are
    separable in the inputs (see remark~\ref{r:separable}), so we can
    avoid some complications around non-anticipative strategies;
    however, they are affine in the inputs and so discontinuous (or
    \emph{bang-bang}) controls are optimal (see
    section~\ref{s:hj}).
  \item The RCBRT is nonconvex and asymmetric, although there is an
    odd symmetry across the $x_3 = 0$ plane.
  \item The surface of the RCBRT is mostly smooth, but is
    non-differentiable along the curve of the crossover points.
  \item Optimal trajectories which lead from points on the boundary of
    the RCBRT to points on the boundary of the target set arise from
    piecewise constant input signals with a bounded number of switches
    for any single trajectory; therefore, it should be possible to
    compute the RCBRT using only this class of input signals even
    though it is defined~\eqref{e:rcbrt-defn} using nonanticipative
    strategies over measurable input signals.
  \item Multiple optimal trajectories can start from a single point on
    the boundary of the RCBRT, terminate at a single point on the
    target set, or both.  Multiple optimal trajectories may share
    segments.  These features are likely to cause problems for
    algorithms which use two point boundary value solvers to search
    for optimal trajectories.
  \end{compactitem}
  We hope that readers will look upon these features as hurdles to
  overcome through design of novel algorithms rather than
  insurmountable obstacles.
\end{remark}
