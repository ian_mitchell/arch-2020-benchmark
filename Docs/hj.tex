%------------------------------------------------------
\section{Approximations from Hamilton-Jacobi-Isaacs}
\label{s:hj}

We describe a formulation of RCBRTs as the sublevel sets of the viscosity solution of a Hamilton-Jacobi-Isaacs (HJI) partial differential equation (PDE) in~\cite{mitchell02,MBT05}, and provide open source \matlab\ code based on level set methods to approximate these solutions in \toolboxLS~\cite{MT05,mitchell07c}.  These approximations may prove useful as another representation of the proposed benchmark RCBRTs against which to compare prospective algorithms, although their memory and computational cost scales exponentially with dimension.  Code for the various extension problems will be added as it becomes available.

%------------------------------------------------------
\subsection{Approximating the Basic Problem's RCBRT}
\label{s:hj-basic}

We start the section with a very brief sketch of the HJI formulation
for the basic problem.  A more in-depth presentation of the
formulation of this particular problem can be found
in~\cite[section~3.1]{mitchell02}, and implementation details are
discussed in~\cite[section~2.6]{mitchell07c}.

The sets are represented by an implicit surface function $\lsf: [-T,
  0] \times \set X \to \R$ such that
\[
  \begin{aligned}
    \set T &= \{ x \in \set X \mid \lsf(0, x) \leq 0 \}, \\
    \reachSet{[-t, 0]}{\set T}
      &= \{ x \in \set X \mid \lsf(t, x) \leq 0 \},
  \end{aligned}
\]
for any $t \in [ 0, T ]$.  We construct $\lsf(0, \cdot)$ from the
definition of $\set T$; in this case
\begin{equation} \label{e:basic-target-lsf}
    \lsf(0, x) = \sqrt{x_1^2 + x_2^2} - \beta.
\end{equation}
The implicit surface function at times $t > 0$ is the solution of the
HJI PDE
\[
    D_t \lsf(t,x) + \min[0, H(x, D_x \lsf(t,x))] = 0.
\]

In this case the Hamiltonian is given by
\begin{equation} \label{e:basic-hamiltonian}
  \begin{aligned}
    H(x, p) &= -\left(
                \max_{\omega_e \in [-\overline \omega, \overline \omega]}
                \min_{\omega_p \in  [-\overline \omega, \overline \omega]}
                \bma p_1 & p_2 & p_3 \ema
                \bma -v_e + v_p \cos x_3 + \omega_e x_2 \\
                      v_p \sin x_3 - \omega_e x_1 \\
                      \omega_p - \omega_e \ema
                \right),\\
      &= -\left(
            \max_{\omega_e \in [-\overline \omega, \overline \omega]}
            \min_{\omega_p \in  [-\overline \omega, \overline \omega]}
            \left[
              \begin{gathered}
                p_1 (-v_e + v_p \cos x_3 + \omega_e x_2) \\
                   + p_2 (v_p \sin x_3 - \omega_e x_1)
                   + p_3 (\omega_p - \omega_e)
              \end{gathered} \right] \right), \\
      &= p_1 (v_e - v_p \cos x_3) - p_2 (v_p \sin x_3) \\
      & \qquad
            -\left(
              \max_{\omega_e \in [-\overline \omega, \overline \omega]}
              \min_{\omega_p \in  [-\overline \omega, \overline \omega]}
              \left[ \omega_e (p_1 x_2 - p_2 x_1 - p_3) + \omega_p p_3
                \right] \right), \\
      &= p_1 (v_e - v_p \cos x_3) - p_2 (v_p \sin x_3)
              -\overline \omega |p_1 x_2 - p_2 x_1 - p_3|
              +\overline \omega |p_3|.
  \end{aligned}
\end{equation}
For $v_e = v_p = \overline \omega = +1$, this results in the
relatively simple $H(x,p) = p_1(1 - \cos x_3) - p_2 \sin x_3 - |p_1
x_2 - p_2 x_2 - p_3| + |p_3|$.  There are a number of additional
parameters to the level set algorithms which must be chosen before
\toolboxLS\ can compute an approximation of this PDE---most notably
bounds on the partial derivatives of $H$ with respect to $p$---but
code is provided and the results are illustrated in
figure~\ref{f:rcbrt}.

%------------------------------------------------------
\subsection{Approximating the Extension RCBRTs}
\label{s:hj-extensions}

In theory the HJI formulation is general enough to represent the
RCBRTs from all of the extensions in section~\ref{s:extension}, but
because level set methods require gridding the state space computation
is only feasible in state space dimensions beyond four or five by
various reformulations, such as those described
in~\cite{MT03,mitchell11,CHVBT18,LCT19}.

\begin{figure}[t]
	\begin{centering}
	\includegraphics[width=0.5\textwidth]{no_heading_surface}
	\caption{The pursuer's choice RCBRT for the two dimensional
          no-heading version.  The outer red solid curve is the RCBRT,
          the inner dotted blue circle is the target set, and the
          intermediate magenta dashed curves are slices of the
          analytic solution (the same slices as
          figure~\ref{f:barrier-slices-evader}, but with $x_1$ and
          $x_2$ swapped).}
	\label{f:no-heading-surface}
	\end{centering}
\end{figure}

\paragraph{The No-Heading Extension.} For this extension from
section~\ref{s:extension-no-heading}, $\set X = \R^2$ (without the
relative heading dimension) and we can easily compute an
approximation of the corresponding HJI PDE.  The target set is defined
as in~\eqref{e:basic-target-lsf}.  We start from the
Hamiltonian~\eqref{e:basic-hamiltonian} and make the following
adjustments:
\begin{compactitem}
\item Any term involving $p_3$ is set to zero.  The justification for
  this cancellation can be found in~\cite{MT03}.
\item The value of $x_3$ in any term is chosen to minimize (to give
  the advantage to the pursuer) the inner product term.  In
  particular, that means finding
  \begin{equation} \label{e:optimal-x3}
      \min_{\theta \in [ -\pi, \pi )} -v_p (p_1 \cos(\theta) - p_2 \sin(\theta)).
  \end{equation}
  A quick bit of calculus can find that the optimal $\theta^*$
  satisfies $\tan(\theta^*) = -p_2 / p_1$, so we evaluate the
  functional for both possible values of $\theta^* \in [ -\pi, \pi)$
    and substitute the one which generates the minimum into the
    Hamiltonian.
\end{compactitem}
The resulting Hamiltonian is
\[
     H(x,p) = p_1 v_e - v_p \left[p_1 \cos(\theta^*) - p_2 \sin(\theta^*)\right]
              -\overline \omega \left|p_1 x_2 - p_2 x_1\right|,
\]
and figure~\ref{f:no-heading-surface} shows the resulting two
dimensional RCBRT.  A few things to note about this version of the
problem:
\begin{compactitem}
\item Although it is not obvious in the derivation above, the
  pursuer's angular velocity $\omega_p$ disappears from the problem.
  Some intuition for this outcome can be gleaned from the fact that we
  are choosing the relative heading $x_3 \in [ -\pi, \pi )$ to be the
    best value for the pursuer.  If the pursuer can always choose the
    best relative heading, then its turn rate is irrelevant.
\item If we seek to give the advantage of the unknown heading to the
  evader then we can find the $\theta$ which maximizes the functional
  in~\eqref{e:optimal-x3}.  It is again not obvious, but for the given
  initial conditions~\eqref{e:basic-target-lsf} the resulting
  Hamiltonian is strictly positive, the solution to the resulting HJI
  PDE is $\lsf(t,x) = \lsf(0,x)$, and the RCBRT is exactly the target
  circle in figure~\ref{f:no-heading-surface}.
\end{compactitem}

\paragraph{Other Extension Cases.} We will add HJI PDE approximations for
these cases as they become available.
