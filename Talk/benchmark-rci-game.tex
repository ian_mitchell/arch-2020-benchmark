\documentclass[10pt]{beamer}

%\setbeameroption{show notes}

\input{header}
\useoutertheme{ubc}
\usecolortheme{ubc}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% START DOCUMENT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{A Robust Controlled Backward Reach Tube\\ with (Almost) Analytic Solution for Two Dubins Cars}
\subtitle{ARCH 2020 Benchmark Proposal}
\date{July 2020}
\author{Ian M. Mitchell}
%\subject{CPSC 303 Lecture Notes}
\institute[UBC Computer Science]{\normalsize Department of Computer Science\\The University of British Columbia}

\begin{document}
\begin{frame}
\hrule
\titlepage

\centering
\vfill
\small 
\href{mailto:mitchell@cs.ubc.ca}{mitchell@cs.ubc.ca}\\
\url{https://www.cs.ubc.ca/~mitchell}
\vfill
\tiny
Copyright 2020 by Ian M. Mitchell\\
This work is made available under the terms of the Creative Commons
Attribution 4.0 International license\\
\url{http://creativecommons.org/licenses/by/4.0/}

%\bigskip
%\tableofcontents

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Motivation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Results 1--10 of 42,000\ldots}

  ``Reachability'' examines whether trajectories of a system reach certain states, but is used in many distinct contexts

  \begin{itemize}
    \item States reached from an initial set (\emph{forward}), or which reach a target set (\emph{backward}).
    \item States reached at a particular time (\emph{reach set}) or at some point during a time interval (\emph{reach tube}).
    \item For systems with inputs, states which may be reached by some input and/or must be reached for all inputs (??).
    \item Distinctions examined in [Mitchell, 2007], but how to find related work?

  \end{itemize}

  \vfill
  We need better labelling for systems with inputs!
  \vfill

\end{frame}
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Robust Controlled Backward Reachable Tube}

  To make it easier to search for results, consider following the lead of the invariant set community:

  \begin{itemize}
    \item \emph{Controlled} for states which can be driven to the target set by some (control) input signal.
    \item \emph{Robust} for states which must lead to the target set no matter what (disturbance) input signal is applied.
  \end{itemize}

  \vfill
  Therefore, \emph{robust controlled backward reach tubes} (RCBRTs) are the most general (non-stochastic) case:
  \begin{itemize}
    \item Some inputs try to reach the target set.
    \item Other inputs try to avoid the target set.
  \end{itemize}

\end{frame}
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Approximating RCBRTs}

  RCBRT approximation algorithms exist but have significant limitations:

  \begin{itemize}
    \item Hamilton-Jacobi-Isaacs PDEs:
      \begin{itemize}
        \item Handle general dynamics and shapes.
        \item No over/under approximation guarantee, scale very poorly.
      \end{itemize}
    \item Viability theory:
      \begin{itemize}
        \item Handle general dynamics and shapes, guarantee over/under approximation.
        \item Scale even more poorly, not publicly released(?).
      \end{itemize}
    \item Ellipsoidal parametric representations:
      \begin{itemize}
        \item Limited to (intersections / unions of) ellipsoids and linear dynamics.
        \item Computationally scalable, but ellipsoids become poor approximations of boxes in high dimension.
      \end{itemize}
    \item Adversarial inputs break techniques based on convex optimization or smart sampling.
  \end{itemize}

  \vfill
  
  \begin{center}
  \textcolor{red}{\Large --- Can you do better? ---}
  \end{center}

\end{frame}

  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Some beamer magic I borrowed from Shahab.  This automatically puts
% the outline slide at the beginning of each section and highlights
% the current section.  Note that it does nothing right here.

% I purposefully do not introduce this magic until after the
% motivation section since I don't want to start the talk with an
% outline.  This way the outline appears first *after* the motivation,
% but the motivation is still listed in the outline.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\AtBeginSection[]%
{
\begin{frame}<beamer>
   \frametitle{Outline}
   
   \tableofcontents[currentsection]
   \addtocounter{framenumber}{-1}
   
\end{frame}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now we put the complete outline, with nothing highlighted.
\begin{frame}{Contribution}

   In this paper we propose a challenging benchmark problem against which to compare new algorithms for RCBRT approximation.

   \begin{columns}
   
   \column{0.5\textwidth}
   \begin{center}
   Outline
   \end{center}   
   \tableofcontents
   
   \column{0.3\textwidth}
	  \begin{center}
	    \includegraphics[height=0.3\textheight]{airSoln1}\\
	    \vfill
	    \includegraphics[height=0.3\textheight]{airSoln2}\\
	    {\small [Mitchell et. al., 2005]}
	  \end{center}
   \end{columns}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Basic Version}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Problem}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Kinematic Unicycle \& Dubins Car Model}

  \begin{columns}[c]
	  \column{0.7\textwidth}
		The motion of a kinematic unicycle model is governed by the ODE
		\[
		    \dot x
		      = \frac{d}{dt} \bma x_1 \\ x_2 \\ x_3 \ema
		      = \bma v \cos x_3 \\ v \sin x_3 \\ \omega \ema
		\]
	  \column{0.3\textwidth}
	  \begin{center}
	    \includegraphics[width=\textwidth]{unicycle-coordinates}
	  \end{center}
  \end{columns}
  	
	\begin{itemize}
	  \item The \emph{state} consists of
	  \begin{itemize}
	    \item The position in the plane $(x_1,x_2) \in \R^2$.
	    \item The heading $x_3 \in [ -\pi, \pi )$.
	    \end{itemize}
	  \item The \emph{inputs} are
	  \begin{itemize}
	    \item Linear velocity $v \in [ \underline v, \overline v ] \subseteq \R$.
	    \item Angular velocity $\omega \in [-\overline \omega, \overline \omega ] \subseteq \R$ (note symmetry of range).
	  \end{itemize}
	  \item In a Dubins car [Dubins, 1957], the linear velocity is fixed.
	  \begin{itemize}
	    \item In this case $v = \underline v = \overline v = +1$.
	  \end{itemize}
	\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Adversarial Game Scenario}

  \begin{itemize}
    \item A \emph{pursuer} agent (red) attempts to cause a collision.
    \item An \emph{evader} agent (blue) attempts to avoid a collision.
    \item Collision occurs if pursuer and evader pass within distance $\beta > 0$, regardless of heading.
    \item Similar to homicidal chauffeur [Isaacs, 1999], but agents are identical Dubins cars.
  \end{itemize}

  \begin{center}
    \includegraphics[width=0.7\textwidth]{air3d-game}
  \end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{State Space Reduction}

  \begin{columns}[c]
    \column{0.45\textwidth}
    Winning conditions depend only on the relative positions of the vehicles, so use a relative coordinate system.
  \[
    \frac{d}{dt} \bma x_1 \\ x_2 \\ x_3 \ema
      = \bma -v_e + v_p \cos x_3 + \omega_e x_2 \\
              v_p \sin x_3 - \omega_e x_1 \\
              \omega_p - \omega_e \ema
  \]
  
    \column{0.45\textwidth}  
    \begin{center}
      \includegraphics[width=0.9\textwidth]{air3d-coordinates}
    \end{center}
  \end{columns}

  \vfill

  \begin{itemize}
	  \item State $( x_1, x_2) \in \R^2$ is the relative position in the plane.
	  \item State $x_3 \in [ -\pi, \pi )$ is the relative heading,
	  \item Fixed parameters $v_e = v_p = +1$ are the linear velocities of the evader and pursuer respectively.
	  \item Inputs $\omega_e$ and $\omega_p$ are the angular velocities of the evader and pursuer respectively.
  \end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Robust Controlled Backward Reach Tube}

RCBRT is
    \[
      \reachSet{[-T,0]}{\set T}
        = \{ x \in \set X \mid
           \exists \omega_p(\cdot), \forall \omega_e\sig,
           \exists s \in [ -T, 0 ], x(s) \in \set T \}
    \]

  \begin{itemize}
	\item $\set X = \R^2 \times [ -\pi, \pi )$ (or $\R^2 \times \mathbb{S}^1$) is the domain.
    \item $\set T = \left\{ x \in \set X \, \left| \,\sqrt{x_1^2 + x_2^2} \leq \beta \right. \right\}$ is the target set.
    \item $T > 0$ is (backward) horizon.
    \item $\omega_p(\cdot)$ is a nonanticipative strategy.
    \item $\omega_e(\cdot)$ is a measurable function from time to input value.
  \end{itemize}

  \begin{center}
    \includegraphics[height=3cm]{basic_surface}\\
    Two views of RCBRT (red).  Target set $\set T$ on left (blue).
  \end{center}
  
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{An (Almost) Analytic Solution}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{The Merz Solution}

  \begin{itemize}
    \item Optimal trajectories deduced in [Merz, 1972] with pursuer at the origin.
    \item Points along the boundary of the RCBRT for chosen slices of $x_3$ can be calculated to arbitrary density.
  \end{itemize}
      
  \begin{center}
    \includegraphics[height=5cm]{pursuer_slices}\\
    Slices of RCBRT (pursuer at origin) for $x_3 = \frac{\pi}{6}, \frac{2\pi}{6}, \frac{3\pi}{6}, \frac{4\pi}{6}, \frac{5\pi}{6}, \pi$
  \end{center}
  
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Evader at the Origin}

  \begin{itemize}
    \item Slice boundary consists of semicircle of $\set T$, \emph{left barrier} and \emph{right barrier}.
    \item Left and right barriers meet at a \emph{crossover point}.
    \item Evader case (and \matlab\ code) in [Mitchell, 2001].
  \end{itemize}
      
  \begin{center}
    \includegraphics[height=5cm]{evader_slices}\\
    Slices of RCBRT (evader at origin) for $x_3 = \frac{\pi}{6}, \frac{2\pi}{6}, \frac{3\pi}{6}, \frac{4\pi}{6}, \frac{5\pi}{6}, \pi$
  \end{center}
  
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Optimal Trajectories}

  \begin{itemize}
    \item Optimal trajectories lead from points on the surface of the RCBRT to points on the target set.
    \item Optimal trajectories consist of segments during which both inputs are fixed with various combinations of $\omega_e \in \{ -1, 0, +1 \}$ and $\omega_p \in \{ -1, +1 \}$.
  \end{itemize}
  
  \begin{center}
    \includegraphics[height=5cm]{pursuer_left_barrier}\\
    Trajectories leading from points on the left barrier (pursuer at origin) for $x_3 = \frac{5\pi}{6}$
  \end{center}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Sneaky, Tricksy Little Trajectories}

  \begin{itemize}
    \item Two distinct trajectories start at each crossover point.
    \item RCBRT surface is not differentiable at crossover points.
    \item Trajectories may or may not lead to same point on target set.
  \end{itemize}
  
  \begin{center}
    \includegraphics[height=5cm]{pursuer_crossover}
    Trajectories leading from crossover points (pursuer at origin) for $x_3 = \frac{5\pi}{6}, \frac{3\pi}{6}$
  \end{center}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Basic Problem Summary}

  Many features which makes this RCBRT interesting:

  \begin{itemize}
    \item Nonlinear (but simple and Lipschitz) dynamics in low dimension.
    \item States $x_1$ and $x_2$ are coupled in dynamics and target set.
    \item Domain $\set X$ is not of the form $\R^d$ for some $d$.
    \item Two, adversarial (albeit separable) inputs.
    \begin{itemize}
      \item Optimal input signals are piecewise constant.
    \end{itemize}
    \item Multiple optimal trajectories can start from a single point on the boundary, terminate at a single point on the target set, or both.
    \item Optimal trajectories may share segments.
    \item Some optimal trajectories lie along the boundary, but most do not.
    \item RCBRT nonconvex, asymmetric and nonsmooth.
    \item Analytic formulas exist to derive points on the boundary to arbitrary density.
    \begin{itemize}
      \item For all points except crossover points, these formulas are explicit.
    \end{itemize}
  \end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Extensions}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{No Heading}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{No Heading Simplification}

  Reduce to 2D by projecting out $x_3$.

  \begin{itemize}
    \item To ensure safety of evader, treat $x_3$ as an input for the pursuer to choose.
    \item Analyzed in [Tomlin \& Mitchell, 2003].
    \item Solution derived by projection of $x_3$.
  \end{itemize}
  
  \begin{center}
    \includegraphics[height=4.5cm]{no_heading_surface}\\
    2D RCBRT (evader at origin) when $x_3$ is pursuer's input
  \end{center}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Absolute Coordinates}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Absolute Coordinates}

  Increase to 6D by working in original coordinates.

  \begin{itemize}
	\item Domain $\set X = \R^2 \times [ -\pi, \pi ) \times \R^2 \times [ -\pi, \pi )$.
	\item Target $\set T = \left\{ x \in \set X \, \left| \, \sqrt{(x_3 - x_1)^2 + (x_4 - x_2)^2} \leq \beta \right. \right\}.$
	\item Dynamics
	  \[
	      \frac{d}{dt} \bma x_1 \\ x_2 \\ x_3 \\ x_4 \\ x_5 \\ x_6 \ema
	      = \bma v \cos x_3 \\ v \sin x_3 \\ \omega_e \\
	             v \cos x_6 \\ v \sin x_6 \\ \omega_p \ema
	  \]
	\item Solution derived by converting to relative coordinates.
  \end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{More Vehicles}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{More Vehicles}

  Increase to arbitrary dimension by adding more pursuers and/or evaders.

  \begin{itemize}
  \item Relative coordinates make some sense if there is a single pursuer or evader.
  \item Absolute coordinates always work.
  \item Target set and/or hybrid state model can capture various winning conditions.
  \begin{itemize}
    \item For example, two pursuers capture two evaders simultaneously:
      \footnotesize
      \[
          \set T = \left\{ x \in \set X \, \left| \,
                     \max \left[
                       \begin{gathered}
                       \min \left(
                         \begin{gathered}
                           \sqrt{(x_1 - x_7)^2 + (x_2 - x_8)^2}, \\
                           \sqrt{(x_1 - x_{10})^2 + (x_2 - x_{11})^2}
                         \end{gathered}
                       \right), \\
                       \min \left(
                         \begin{gathered}
                           \sqrt{(x_4 - x_7)^2 + (x_5 - x_8)^2}, \\
                           \sqrt{(x_4 - x_{10})^2 + (x_5 - x_{11})^2}
                         \end{gathered}
                       \right)
                       \end{gathered}
                       \right] \leq \beta
                       \right. \right\},
      \]
      \normalsize
    \end{itemize}
    \item Solution or underapproximation sometimes available.
    \begin{itemize}
      \item For example, a single pursuer capturing one of multiple evaders can be deduced by pairwise analysis.
    \end{itemize}
  \end{itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Unicycle Kinematics or Dynamics}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Unicycle Kinematics or Dynamics}

  Increase complexity by adding inputs or state dimension by adding inertia.

  \begin{itemize}
    \item Full kinematic model allows agents to choose linear velocities $v_p$ and $v_e$ as well as angular velocities $\omega_p$ and $\omega_e$.
    \item Dynamic model shifts input authority to accelerations / forces.
    \begin{itemize}
      \item For example, dynamics (in relative coordinates) for agents with non-trivial inertia but negligible moment of inertia:
      \[
        \frac{d}{dt} \bma x_1 \\ x_2 \\ x_3 \\ x_4 \\ x_5 \ema
        = \bma -x_4 + x_5 \cos x_3 + \omega_e x_2 \\
              x_5 \sin x_3 - \omega_e x_1 \\
              \omega_p - \omega_e \\
              a_e \\ a_p \ema,
      \]
      with inputs $\omega_p$, $\omega_e$, $a_p$ and $a_e$.
    \end{itemize}
    \item No simple method to extend Merz analysis to these cases.
  \end{itemize}
  
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{Conclusions}

  \begin{itemize}
    \item Robust controlled backward reach tubes (RCBRTs) are powerful tools for analyzing the behaviour of systems.
      \begin{itemize}
        \item Naming convention ``controlled'' or ``robust controlled'' backward reach tubes follows lead of invariant set community.
        \item Adopt this convention to make it easier to find papers!
      \end{itemize}
    \item Game of two identical vehicles provides a challenging RCBRT benchmark problem with an (almost) analytic solution against which to compare.
      \begin{itemize}
        \item Numerous features to stretch the limits of reachability algorithms.
        \item Hamilton-Jacobi-Isaacs PDEs provide approximate solutions but do not currently scale or guarantee under / over approximation.
        \item Various extensions to higher dimension are available.
      \end{itemize}      
    \item Source and updates at 
      \begin{centering}
        \url{https://bitbucket.org/ian_mitchell/arch-2020-benchmark}
      \end{centering}
  \end{itemize}

\end{frame}

\ignore{
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{}

  \begin{itemize}
  \item
  \end{itemize}
  
  \begin{center}
    %\includegraphics[height=5cm]{}
  \end{center}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{}

  \begin{itemize}
  \item
  \end{itemize}
  
  \begin{center}
    %\includegraphics[height=5cm]{}
  \end{center}

\end{frame}

} % \ignore


\end{document}
