# Code/ #


The subdirectories contain code for various versions of the robust controlled backwards reach tubes (RCBRTs) for the game of two Dubins cars and various extensions.

[This repository](https://bitbucket.org/ian_mitchell/arch-2020-benchmark/) contains this code as well as the associated ARCH 2020 paper describing the benchmark and slides for the corresponding talk.

### Analytic/ ###

Matlab code to recreate the (almost) analytic solution based on [Merz, 1972](https://doi.org/10.1007/BF00932932).  Two versions are available in subdirectories:

* Evader at the origin (corresponding to most of the figures in the benchmark paper).

* Pursuer at the origin (corresponding to Merz's original formulation).

This code can be used to recreate figures 3-6 in the benchmark paper.  This code is essentially verbatim from the [code](https://www.cs.ubc.ca/~mitchell/Downloads/merz.zip) released with [Mitchell, 2001](https://www.cs.ubc.ca/~mitchell/Papers/merz.pdf).

### HJI-Approximations/ ###

Matlab code using [ToolboxLS](https://www.cs.ubc.ca/~mitchell/ToolboxLS) to approximate the solution of the RCBRT using an Hamilton-Jacobi-Isaacs PDE formulation.  Can be used to recreate figures 2 and 7 in the benchmark paper.

### ToolboxLS-Kernel/ ###

A copy of the Kernel/ subdirectory of [ToolboxLS](https://www.cs.ubc.ca/~mitchell/ToolboxLS) to make this repository self-contained.  Used by the code in HJI-Approximations/.

